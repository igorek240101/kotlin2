// 1. Afterward, add the data modifier to the resulting class. The compiler will generate a few useful methods for this class: equals/hashCode, toString, and some others.
data class Person

constructor(val name: String, val age : Int)

fun getPeople(): List<Person> {
    return listOf(Person("Alice", 29), Person("Bob", 31))
}

fun comparePeople(): Boolean {
    val p1 = Person("Alice", 29)
    val p2 = Person("Alice", 29)
    return p1 == p2  // should be true
}
// 2. Rewrite the following Java code using smart casts and the when expression:
fun eval(expr: Expr): Int =
        when (expr) {
            is Num -> expr.value 
            is Sum -> eval(expr.left) + eval(expr.right)
            else -> throw IllegalArgumentException("Unknown expression")
        }

interface Expr
class Num(val value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr
// 3. Reuse your solution from the previous task, but replace the interface with the sealed interface. Then you no longer need the else branch in when.
fun eval(expr: Expr): Int =
        when (expr) {
            is Num -> expr.value 
            is Sum -> eval(expr.left) + eval(expr.right)
        }

sealed interface Expr
class Num(val value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr
// 4. Uncomment the code and make it compile. Rename Random from the kotlin package to KRandom, and Random from the java package to JRandom.
 import kotlin.random.Random as KRandom
 import java.util.Random as JRandom

fun useDifferentRandomClasses(): String {
    return "Kotlin random: " +
             KRandom.nextInt(2) +
            " Java random:" +
             JRandom().nextInt(2) +
            "."
}
// 5. Then implement the extension functions Int.r() and Pair.r() and make them convert Int and Pair to a RationalNumber.
fun Int.r(): RationalNumber = RationalNumber(this, 1);

fun Pair<Int, Int>.r(): RationalNumber = RationalNumber(first, second)

data class RationalNumber(val numerator: Int, val denominator: Int)